import React from 'react';
import TodoListItem from "../todo-list-item";
import './todo-list.css'

const TodoList = (props) => {
  const elements = props.todos.map((item) => {
    return (
        <li key={item.id}><TodoListItem task={item.label} /></li>
    )
  })

  return(
      <div>
        <ul>
          {elements}
        </ul>
      </div>
  )
}
export default TodoList;