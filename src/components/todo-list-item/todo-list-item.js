import React from 'react';
import "./todo-list-item.css"

const TodoListItem = (props) => {
  const {task} = props;
  return (
        <span>
        {task}
      <button type="button"
        className="btn btn-outline-success btn-sm float-right"
>
        <i className="fas fa-exclamation-circle"/>
      </button>

      <button type="button"
        className="btn btn-outline-danger btn-sm float-right"
>
        <i className="fas fa-trash" />
      </button>
    </span>

  );
};



export default TodoListItem;