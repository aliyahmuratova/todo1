import React from 'react';
import './todo-status-filter.css'

const TodoStatusFilter = () => {
  return (
      <div className='btn'>
        <button>All</button>
        <button>Active</button>
        <button>Done</button>
      </div>
  )
}
export default TodoStatusFilter;