import React from 'react';
import TodoHeader from '../todo-header';
import TodoSearchPanel from "../todo-search-panel";
import TodoStatusFilter from "../todo-status-filter";
import TodoList from "../todo-list";

import './app.css'

const App = () => {
  const todos = [
  {
    id: 1,
    label: "Buy milk",
    done: false,
    important: false,
  },
  {
    id: 2,
    label: "Walk the dog",
    done: false,
    important: false,
  },
  {
    id: 3,
    label: "Go to work",
    done: false,
    important: false,
  },
  {
    id: 4,
    label: "Sleep",
    done: false,
    important: false,
  }
]

  return (
    <div className='test'>
      <TodoHeader />
      <TodoSearchPanel />
      <TodoStatusFilter />
      <TodoList todos = {todos} />

    </div>
  )
}
export default App;